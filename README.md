# MM-NLG 2023 Website
The website is automatically put on server when you make a commit. You can check if the website was deployed successfully here: https://gitlab.inria.fr/synalp/mmnlg2023/-/pipelines

If there was a problem, see the log.

To develop the website locally, follow the instructions below.

## Installation
```
pip install mkdocs
pip install mkdocs-material
```

## Build the website
```mkdocs build [--clean]```

The result will be store in the site/ directory.
Every time this directory is commited and pushed to the original repository on gitlab, it is copied to gitlabpages.inria.fr as well.


## Start the development server
```mkdocs serve```
