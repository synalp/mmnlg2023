# Invited Speakers

## [Sandro Pezzelle](https://sandropezzelle.github.io)

![Sandro Pazzelle](images/sandro2022rid2.jpg)

Assistant Professor in Responsible AI at the ILLC, Faculty of Science, University of Amsterdam. His research combines NLP, computer vision, and cognitive science, with focuses on visually-grounded models, computational semantics and pragmatics, and interpretability. Sandro was previously a Postdoc working on the DREAM (Distributed dynamic REpresentations for diAlogue Management) ERC project led by Raquel Fernández. He completed his PhD at CIMeC, University of Trento, under the supervision of Raffaella Bernardi. Sandro is also a member of the ELLIS society, a faculty member of the ELLIS Amsterdam Unit, a board member of SigSem, the ACL special interest group in computational semantics, and part of TACL editorial team.

**Title:** Adaptation in the Realm of Visually Grounded Language: Of Speakers, Models, and Tasks

**Abstract:** When communicating, speakers adapt their language to those of their interlocutors. This can also be achieved by intelligent NLP systems, particularly by NLG models adapting their outputs based on the features of a given context. More broadly, NLG models open up new possibilities for adapting tasks and evaluations, which allows us to study language in a more naturalistic setting. Roaming the realm of visually grounded language, I will present works that explore adaptation from each of these perspectives. First, I will talk about how human speakers can adapt to their interlocutor's semantic interpretation of context-dependent expressions, and how this is boosted by an active dialogical interaction. Second, I will move to NLG models and show that pre-trained agents conditioned on the fly can adapt to the knowledge of their interlocutors. Finally, I will dive into some language-and-vision tasks, and show preliminary results indicating the potential of adapting standard tasks to leverage the effectiveness of last-generation generative models.


## [Dimitra Gkatzia](https://www.napier.ac.uk/people/dimitra-gkatzia)

![Dimitra Gkatzia](images/dimitra.jpg)

Dimitra Gkatzia is an Associate Professor at the School of Computing at Edinburgh Napier University. She is interested in exploring data-driven Natural Language Generation (NLG) for low-resource domains/languages as well as Human-Robot Interaction, where she investigates the interplay between various modalities (vision, speech, knowledge-bases) in real-world settings for human-robot teaming scenarios. Since 2021, Dimitra is the co-lead of the SICSA (Scottish Informatics and Computer Science Alliance) AI Theme. Between 2016-2020, Dimitra served as an elected member of the Steering Board of the Special Interest Group in Natural Language Generation (SIGGEN). At Edinburgh Napier, Dimitra is currently leading the Natural Language Processing Group. 

**Title:** Dealing with Data Challenges in  Low-Resource NLG

**Abstract:** Many domains and tasks in natural language generation (NLG) are inherently ‘low-resource’, where data and linguistic tools are scarce if not absent. This poses a particular challenge to researchers and system developers in the era of machine-learning-driven NLG, but also calls for more efficient data collection procedures. In this talk, I will initially present the challenges researchers & developers often encounter when dealing with low-resource settings in NLG and discuss current approaches to low-resource NLG. I will then introduce our challenge proposal for the NLG community: low-resource language corpus development (LowRe), a framework designed to collect a single dataset with dual tasks to maximize the efficiency of data collection efforts and respect participants' time.  Finally, I will present a case study with Scottish Gaelic using the LowRe framework.