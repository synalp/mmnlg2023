# Workshop on Multimodal, Multilingual Natural Language Generation

!!! Info
    People attending the workshop should register via the [registration link for INLG 2023](https://inlg2023.github.io/).

!!! Info
    MM-NLG will accept papers which have already been reviewed for the main INLG or SIGDIAL conferences. Authors who wish to submit papers rejected for the main conferences can do so via the [MMNLG START submission system](https://softconf.com/n/mmnlg2023/), indicating that this is a resubmission from the main conference. We will then consider the reviews the paper received for the main conference.


We invite the submission of long and short papers for the first Workshop on Muiltimodal, Multilingual NLG (MM-NLG), which will be held in Prague, in conjunction with the joint meetings of the [16th International Conference on Natural Language Generation (INLG 2023)](https://inlg2023.github.io/) and the [24th Annual Meeting of the Special Interest Group on Discourse and Dialogue (SIGDial 2023)](https://2023.sigdial.org/). 

## Workshop goals and topics
This event aims to bring together researchers working on text generation from multimodal input data. The workshop also emphasises multilinguality as an ongoing, open challenge for text generation methods, especially for languages which are relatively under-resourced.
We therefore invite papers on all topics related to text generation from multimodal inputs, multilingual text generation, or a combination of the two. We welcome submissions which focus on multimodal and/or multilingual generation in both dialogue and non-interactive settings.

### NLG and multimodal inputs
By Multimodal NLG, we intend to capture a broad variety of input data types and formats from which text can be generated using neural, statistical or rule-based methods. For example, while several contemporary NLG models generate based on textual prompts or prefixes, others rely on structured inputs which can take the form of `flat' semantic representations, RDF triples, etc. In a different vein, vision-to-text models generate captions, paragraphs or short narratives from visual inputs such as images or video. Finally, there is a long tradition in data-to-text NLG which seeks to generate text from numerical or other, less structured inputs. The sheer diversity is also reflected in the broad range of datasets available for training and evaluating NLG models.

This workshop will provide a forum to discuss NLG research based on any input modality, fostering a debate on the directions in which the field has developed, and especially the relationship between different NLG tasks, as characterised by the variety of possible inputs, among others.

### NLG and multilingual outputs
As the field has become increasingly dominated by large, pretrained language models, it has become increasingly evident that not all languages are on a level playing field. For example, when training data is opportunistically sourced from the web, data for certain languages is often very limited, and highly noisy. On the other hand, developing curated multilingual data for under-represented languages is very challenging, as some recent efforts (for example, the BLOOM model) have shown.

This workshop will provide an opportunity for researchers to discuss challenges and report on recent work targeting NLG in multiple languages, including, but not limited to, data-lean scenarios, where transfer learning, few-shot and zero-shot approaches would be expected to play an important role.

## Workshop format
This one-day workshop will consist entirely of oral presentations, together with a special session for the WebNLG challenge. The oral session will feature talks by two invited speakers, as well as regular paper presentations.

### In-person vs hybrid
The workshop will be hybrid. We encourage all participants to be present, but will provide online access for those who are unable, or prefer not to travel.

### Special session: WebNLG Challenge on Under-Resourced Languages
In line with the goals of MM-NLG, the workshop will include a special session dedicated to the recently launched, ongoing WebNLG 2023 Challenge, which focuses on generation for under-resourced languages in few-shot and zero-shot settings.

## Important dates
* Deadline for long and short papers: 18 July, 2023
* Notification of acceptance:  6 August, 2023
* Deadline for camera-ready papers: 14 August, 2023
* MM-NLG Workshop: 12 September, 2023

