# Organising Committee

* Anya Belz, ADAPT, Dublin City University, Ireland 
* Claudia Borg, University of Malta, Malta
* Liam Cripwell, CNRS/LORIA and Lorraine University, France
* Aykut Erdem, Koc University, Turkey
* Erkut Erdem, Hacettepe University, Turkey
* Claire Gardent, CNRS/LORIA, France
* Albert Gatt, Utrecht University, The Netherlands
* John Judge, ADAPT, Dublin City University, Ireland
* William Soto-Martinez, CNRS/LORIA and Lorraine University, France

## Support and acknowledgements
This workshop is a joint initiative which has received the support of the following projects:

* LT-Bridge funded by the EU  Horizon 2020 Work Programme Spreading Excellence and Widening Participation (WIDESPREAD) 2018-2020 Grant No. 952194;
* The xNLG AI Chair on Multilingual, Multi-Source Text Generation funded by the French National Research Agency (Gardent; ANR-20-CHIA-0003), Meta and the Region Grand Est.
* Multi3Generation: Multimodal, Multi-task, Multi-Lingual Natural Language Generation COST Action CA18231.