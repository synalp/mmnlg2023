# Accepted Papers

Confidently Wrong: Exploring the Calibration and Expression of (Un)Certainty of Large Language Models in a Multilingual Setting

* _Lea Krause, Wondimagegnhue Tufa, Selene Baez Santamaria, Angel Daza, Urja Khurana and Piek Vossen_

Visual Question Generation in Bengali

* _Mahmud Hasan, Labiba Islam, Jannatul Ruma, Tasmiah Mayeesha and Rashedur Rahman_

Keeping an Eye on Context: Attention Allocation over Input Partitions in Referring Expression Generation

* _Simeon Schüz and Sina Zarrieß_

Are Language-and-Vision Transformers Sensitive to Discourse? A Case Study of ViLBERT

* _Ekaterina Voloshina, Nikolai Ilinykh and Simon Dobnik_

Using Large Language Models for Zero-Shot Natural Language Generation from Knowledge Graphs

* _Agnes Axelsson and Gabriel Skantze_



## WebNLG 

The 2023 WebNLG Shared Task on Low Resource Languages. Overview and Evaluation Results (WebNLG 2023)

* _Liam Cripwell, Anya Belz, Claire Gardent, Albert Gatt, Claudia Borg, Marthese Borg, John Judge, Michela Lorandi, Anna Nikiforovskaya and William Soto Martinez_

WebNLG-Interno: Utilizing FRED-T5 to address the RDF-to-text problem (WebNLG 2023)

* _Maxim Kazakov, Julia Preobrazhenskaya, Ivan Bulychev and Aleksandr Shain_

Better Translation + Split and Generate for Multilingual RDF-to-Text (WebNLG 2023)

* _Nalin Kumar, Saad Obaid Ul Islam and Ondrej Dusek_

Data-to-text Generation for Severely Under-Resourced Languages with GPT-3.5: A Bit of Help Needed from Google Translate (WebNLG 2023)

* _Michela Lorandi and Anya Belz_

DCU/TCD-FORGe at WebNLG’23: Irish rules! (WegNLG 2023)

* _Simon Mille, Elaine Uí Dhonnchadha, Stamatia Dasiopoulou, Lauren Cassidy, Brian Davis and Anya Belz_

WebNLG Challenge 2023: Domain Adaptive Machine Translation for Low-Resource Multilingual RDF- to-Text Generation (WebNLG 2023)

* _Kancharla Aditya Hari, Bhavyajeet Singh, Anubhav Sharma and Vasudeva Varma_