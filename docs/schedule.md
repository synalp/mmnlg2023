# Schedule

* 09:00 - 09:15 &emsp;&emsp;Opening Remarks
* 09:15 - 10:45 &emsp;&emsp;Oral presentations: Long papers
    - Are Language-and-Vision Transformers Sensitive to Discourse? A Case Study of ViLBERT 
        * Authors:  Ekaterina Voloshina, Nikolai Ilinykh and Simon Dobnik
    - Using Large Language Models for Zero-Shot Natural Language Generation from Knowledge Graphs 
        * Authors:  Agnes Axelsson and Gabriel Skantze
    - Visual Question Generation in Bengali 
        * Authors:  Mahmud Hasan, Labiba Islam, Jannatul Ferdous Ruma, Tasmiah Tahsin Mayeesha and Rashedur M. Rahman
* 10:45 - 11:15 &emsp;&emsp;Coffee break
* 11:15 - 12:30 &emsp;&emsp;Invited talk: Dimitra Gkatzia 
* 12:30 - 14:00 &emsp;&emsp;Lunch break
* 14:00 - 15:00 &emsp;&emsp;Invited talk: Sandro Pezzelle
* 15:00 - 15:30 &emsp;&emsp;Oral presentations: Short papers
    - Confidently Wrong: Exploring the Calibration and Expression of (Un)Certainty of Large Language Models in a Multilingual Setting 
        * Authors:  Lea Krause, Wondimagegnhue Tsegaye Tufa, Selene Baez Santamaria, Angel Daza, Urja Khurana and Piek Vossen
    - Keeping an Eye on Context: Attention Allocation over Input Partitions in Referring Expression Generation
        * Authors:  Simeon Schüz and Sina Zarrieß
* 15:30 - 16:00 &emsp;&emsp;Coffee break
* 16:00 - 17:30 &emsp;&emsp;WebNLG 2023 Special Session
    - Better Translation + Split and Generate for Multilingual RDF-to-Text
        * Authors: Nalin Kumar, Saad Obaid ul Islam and Ondrej Dusek
    - DCU/TCD-FORGe at WebNLG'23: Irish rules!
        * Authors: Simon Mille, Elaine Ui Dhonnchadha, Stamatis Dasiopoulou, Lauren Cassidy, Brian Davis and Anya Belz
    - WebNLG-Interno: Utilizing FRED-T5 to address the RDF-to-text problem
        * Authors: Maxim Kazakov, Julia Preobrazhenskaya, Ivan Bulychev and Aleksandr Shain
    - WebNLG Challenge 2023: Domain Adaptive Machine Translation for Low-Resource Multilingual RDF-to-Text Generation
        * Authors: Kancharla Aditya Hari, Bhavyajeet Singh, Anubhav Sharma and Vasudeva Varma
    - Data-to-text Generation for Severely Under-Resourced Languages with GPT-3.5: A bit of Help Needed from Google Translate
        * Authors: Michela Lorandi and Anya Belz


## Important dates
* Deadline for long and short papers: 16 July, 2023
* Notification of acceptance:  6 August, 2023
* Deadline for camera-ready papers: 14 August, 2023
* MM-NLG Workshop: 12 September, 2023