# Call for Papers

## Submission formats
We solicit two kinds of papers:

* Long papers must not exceed eight (8) pages of content, plus unlimited pages of ethical considerations, supplementary material statements, and references. 
* Short papers must not exceed four (4) pages, plus unlimited pages of ethical considerations, supplementary material statements, and references. 

Submissions should follow [ACL Author Guidelines](https://www.aclweb.org/adminwiki/index.php?title=ACL_Author_Guidelines) and policies for submission, review and citation, and be anonymised for double blind reviewing. Please use ACL 2023 style files; LaTeX style files and Microsoft Word templates are available at https://2023.aclweb.org/calls/style_and_formatting/.

Authors must honour the ethical code set out in the [ACL Code of Ethics](https://www.aclweb.org/portal/content/acl-code-ethics). If your work raises any ethical issues, you should include an explicit discussion of those issues. This will also be taken into account in the review process. You may find this [checklist](https://aclrollingreview.org/responsibleNLPresearch/) of use.

Authors are strongly encouraged to ensure that their work is reproducible; see, e.g., the following [reproducibility checklist](https://2021.aclweb.org/calls/reproducibility-checklist/). Papers involving any kind of experimental results (human judgments, system outputs, etc) should incorporate a data availability statement into their paper. Authors are asked to indicate whether the data is made publicly available. If the data is not made available, authors should provide a brief explanation why. (E.g. because the data contains proprietary information.) A [statement guide](https://inlg2023.github.io/resource_statement.html) is available on the INLG 2023 website.

## Paper submission
The workshop will only accept direct submissions. Submissions can be made to the [MM-NLG START website](https://softconf.com/n/mmnlg2023/).

Accepted papers will be published in the Workshop proceedings on the ACL Anthology.

## Important dates
* Deadline for long and short papers: 18 July, 2023
* Notification of acceptance: 6 August, 2023
* Deadline for camera-ready papers: 14 August, 2023
* MM-NLG Workshop: 12 September, 2023